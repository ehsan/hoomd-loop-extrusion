#! /usr/bin/env python3
import hoomd
import numpy as np, os
import saw, hoomd_sbs_utils
from hoomd import md
import argparse
import ehsan_loopextrusion

parser = argparse.ArgumentParser()
args = hoomd_sbs_utils.add_arguments(parser)
beads_list = np.arange(args.N_beads)
##############################
## logging functions ##
##############################

##############################
## command line arguments ##
##############################
N_beads = args.N_beads    # Number of beads on the chain
R_beads = 0.5  # beads radius
sigma = 2*R_beads   # Not clear? the diameter of beads!
D = args.D
if D == 0.0:
    D = 1.5*sigma*(N_beads**0.5887)  # the size of the simulation box
Dhalf = 0.5*D
print('D =', D)
sim_id = args.sim_id
particle_types = ['A', 'B', 'LAD']
particle_types = ['A']

if args.sl_stop_file == None:
    stop_prob_up = np.zeros(N_beads)
    stop_prob_down = np.zeros(N_beads)
else:
    sl_stop = np.genfromtxt(args.sl_stop_file)
    stop_prob_up = sl_stop[:, 0]
    stop_prob_down = sl_stop[:, 1]

if args.sl_stop_prob > 0.0:
    ids = np.arange(N_beads)[stop_prob_up < 1]
    ids2 = np.random.choice(ids, replace=False, size=int(ids.size*args.sl_stop_prob))
    stop_prob_up[ids2] = stop_prob_down[ids2] = 1.0
stop_probs = np.zeros_like(sl_stop)
stop_probs[:, 0] = stop_prob_up
stop_probs[:, 1] = stop_prob_down
print('Number of cTCFs: %d' % (np.sum(stop_probs[:, 0])))
##############################
# simulation parameters #
##############################
np.random.seed(None)
Dhalf = 0.5*D
dt = args.dt  # the integration time step
KT = 1.0    # temperature
dump_period0 = 1e2
if args.hoomd_args == 'cpu':
    hoomd.context.initialize('--mode=cpu')
else:
    hoomd.context.initialize('--mode={}'.format(args.hoomd_args))

##############################
# start or restart? #
##############################
sim_inf = 'N{}-D{:.2f}'.format(N_beads, D)
restart_fname = '{}/restart-{}-{}.gsd'.format(args.restart_path, args.sim_id, sim_inf)
if os.path.exists(restart_fname):
    # read the restart file
    system = hoomd.init.read_gsd(filename=restart_fname, restart=restart_fname)
    restart = True  # flag the simulation mood!
else:
    restart = False  # flag the simulation mood!
    snapshot = hoomd.data.make_snapshot(N=N_beads, box=hoomd.data.boxdim(L=D), particle_types=particle_types, bond_types=['polymer', 'loop_extrusion'])
    beads_positions, img = saw.get_init_pol_xyz(N=N_beads, sigma=sigma, D=D)
    unwrapped_position = beads_positions + img*D
    rcm = np.mean(unwrapped_position, axis=0)
    central_pos = unwrapped_position - rcm
    snapshot = hoomd_sbs_utils.set_snapshot(snapshot, N_beads = N_beads, N_binders=0, sigma=sigma, Dhalf=Dhalf, beads_position=central_pos, img=np.zeros_like(img))
    snapshot.particles.charge[:] = stop_probs[:, 0]
    # snapshot = hoomd_sbs_utils.set_snapshot (snapshot, N_beads = N_beads, N_binders=0, sigma=sigma, Dhalf=Dhalf, beads_position=beads_positions, img=img)
    system = hoomd.init.read_snapshot(snapshot)  # read the first snapshot

# Neighborlist
# nl = md.nlist.tree(check_period=1)
nl = md.nlist.cell(check_period=1)
# nl = md.nlist.stencil(check_period=1)
nl.set_params(r_buff=0.8)
nl.tune()
# nl.reset_exclusions(['1-2'])
##############################
# DPD and FENE #
##############################
dpd = md.pair.dpd(r_cut=1.0, nlist=nl, kT=0.0*KT, seed=1)
dpd.pair_coeff.set(particle_types, particle_types, A=30, gamma=2.0)
#fene = md.bond.harmonic()
link = md.bond.harmonic()
fene = md.bond.fene()
#link = md.bond.fene()
fene.bond_coeff.set('polymer', k=30.0*KT, r0=1.6*sigma, sigma=sigma, epsilon=1)
fene.bond_coeff.set('loop_extrusion', k=0, r0=1.6*sigma, sigma=sigma, epsilon=1)
link.bond_coeff.set('polymer', k=0, r0=sigma)
link.bond_coeff.set('loop_extrusion', k=20, r0=1.2*sigma)
#fene.bond_coeff.set('polymer', k=30, r0=sigma)
#fene.bond_coeff.set('link', k=0, r0=1.5*sigma)
#link.bond_coeff.set('polymer', k=0, r0=1.6*sigma, sigma=sigma, epsilon=1)
#link.bond_coeff.set('loop_extrusion', k=10*KT, r0=1.6*sigma, sigma=1*sigma, epsilon=1)
all = hoomd.group.all()
##############################
# Walls #
if args.wall:
    #wall1 = md.wall.sphere(r=Dhalf*0.8, origin=(0,0,0))
    wall1 = md.wall.plane(origin=(0,0,-Dhalf*1), normal=(0,0,1))
    wall2 = md.wall.plane(origin=(0,0,Dhalf*1), normal=(0,0,-1))
    #wall2 = md.wall.sphere(r=Dhalf, origin=(0,0,+50))
    walls = md.wall.group([wall1])
    walls2 = md.wall.group([wall2])
    lj_wall = md.wall.lj (walls, r_cut=2.0)
    lj_wall.force_coeff.set(particle_types, sigma=1.0, epsilon=1.0, r_cut=2**(1./6.))
    lj_wall2 = md.wall.lj (walls2, r_cut=2.0)
    lj_wall2.force_coeff.set(particle_types, sigma=1.0, epsilon=1.0, r_cut=2**(1./6.))

##############################
## removing overlaps ##
##############################
if not(restart):
    fire = md.integrate.mode_minimize_fire(dt=0.001, ftol=1e-5, Etol=1e-7)
    nve=md.integrate.nve(group=all, limit=1)
    while not(fire.has_converged()):
        hoomd.run(5000)
    nve.disable()
##############################
## normal MD: thermalization ##
##############################
dpd.disable()
## Shifted LJ: WCA repulsive potential
r_cut=sigma*2**(1./6.)
slj = md.pair.lj (r_cut=r_cut, nlist=nl)
slj.pair_coeff.set(particle_types, particle_types, epsilon=1, sigma=sigma)
#slj.pair_coeff.set('B', 'B', epsilon=4, sigma=sigma, r_cut=2.)
slj.set_params(mode='shift')

    
md.integrate.mode_standard(dt=dt)
langevin  = md.integrate.langevin(group=all, kT=1.0*KT, seed=np.random.randint(100000)+1)
for p0 in particle_types:
    langevin.set_gamma(p0, gamma=args.gamma)
##### slip-link
hoomd.run_upto(args.thermal_time)
#####
N_loop_extrusions = args.N_loop_extrusions
loop_extrusions = ehsan_loopextrusion.LEagents(system=system, stop_probs=stop_probs, N_le=N_loop_extrusions, k_off=0.8, move_dist_thr=-1, update_period=args.sl_period, CTCF_type='random')
loop_extrusions.sl_type = 'loop_extrusion'
loop_extrusions.sl_typeid = 1
#replace_loop_extrusion = hoomd.analyze.callback(callback=loop_extrusions.replace_loop_extrusion_factors, period = args.new_sl_period)
loop_extrusion = hoomd.analyze.callback(callback = loop_extrusions.update_loop_extrusion2, period = args.sl_period)
if not(restart):
    snap = system.take_snapshot(bonds=True)
    snap.bonds.resize(snap.bonds.N+N_loop_extrusions)
    sl_indices = np.random.choice(args.N_beads-1, N_loop_extrusions, replace=False)
    for i, a1 in enumerate(sl_indices):
        snap.bonds.group[-i-1] = [a1,a1+1]
        snap.bonds.typeid[-i-1] = 1
        print(i+1, a1, a1+1)
    print('bonds = %d'%snap.bonds.N)
    snap.particles.charge[:] = loop_extrusions.stop_probs[:,0]
    system.restore_snapshot(snap)
hoomd.dump.gsd(args.dump_fname+'-{}.gsd'.format(args.sim_id), period=args.dump_period, group=all, overwrite=not(restart), phase=0, dynamic=['attribute', 'topology','momentum','property'])
restart_gsd = hoomd.dump.gsd(filename=restart_fname+'-{}.gsd'.format(args.sim_id), group=all, truncate=True, period=args.restart_period, phase=0)
#####
#restart_gsd.write_restart()
#for i, p0 in enumerate(particle_types):
#    slj.pair_coeff.set(p0, p0, epsilon=0.5*i+1.5, sigma=sigma, r_cut=2.)
if args.wall:
    lj_wall.force_coeff.set('LAD', sigma=1.0, epsilon=12.0, r_cut=2.0)
    
hoomd.run_upto(args.run_time)
restart_gsd.write_restart()
