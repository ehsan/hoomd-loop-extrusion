import hoomd
import numpy as np
import argparse

def set_snapshot (snapshot, N_beads, N_binders, sigma, Dhalf, beads_position, img, N_chains=1, ex_bond_file=None):
    D=2*Dhalf
    snapshot.particles.diameter[:] = sigma
    snapshot.particles.typeid[:N_beads*N_chains] = np.repeat(range(N_chains), N_beads)   # beads
    snapshot.particles.typeid[N_beads*N_chains:] = N_chains   # binders
    N_bonds = (N_beads-1)*N_chains
    if ex_bond_file!=None:
        ex_bonds = np.genfromtxt(ex_bond_file)
        N_bonds += len(ex_bonds)
    snapshot.bonds.resize(N_bonds)    # set the number of bonds to N_beads-1
    bond_id = 0
    for bead_id in range(0, N_beads*N_chains):
        snapshot.particles.position[bead_id] = beads_position[bead_id]
        snapshot.particles.image[bead_id] = img[bead_id]
        out_box = np.abs(beads_position[bead_id])>Dhalf
        if out_box.any():
            print ('hallo! ',bead_id, beads_position[bead_id])

        if ((bead_id+1) %  N_beads)!=0:
            snapshot.bonds.group[bond_id] = [bead_id, bead_id+1] #connect particles through bonds!
            bond_id += 1
    # extra bonds
    if ex_bond_file!=None:
        for bond0 in ex_bonds:
            snapshot.bonds.group[bond_id] = bond0
            snapshot.bonds.typeid[bond_id] = 2
            bond_id += 1

    # set binders positions
    for binder_id in range(N_beads*N_chains, N_beads*N_chains+N_binders):
        snapshot.particles.position[binder_id] = np.random.rand(3)*D - Dhalf

    return snapshot
#################################
def add_arguments (parser):
    parser.add_argument('-N', '--N_beads', default=10, type=int, dest='N_beads')
    parser.add_argument('--Nsl', '--N-loop-extrusions', default=1, type=int, dest='N_loop_extrusions')
    parser.add_argument('--D', default=0, type=float, dest='D')
    parser.add_argument('--sim-id', '--sim_id', default='', dest='sim_id')
    parser.add_argument('--run-time', '--run_time', default=5e6, type=float, dest='run_time')
    parser.add_argument('--thermalize-time', '--thermal_time', default=1e7, type=float, dest='thermal_time')
    parser.add_argument('--dump-fname', '--dump_fname', default='trajectories.gsd', dest='dump_fname')
    parser.add_argument('--dump-period', '--dump_period', default=5e6, type=float, dest='dump_period')
    parser.add_argument('--le-period', '--sl_period', default=1e3, type=float, dest='sl_period')
    parser.add_argument('--le-koff', default=0.0125, type=int, dest='le_koff')
    parser.add_argument('--le-stop-file', default=None, type=str, dest='sl_stop_file')
    parser.add_argument('--le-extra-stop-prob', '--le-stop-prob', default=0.0, type=float, help='sl stop probability at random places', dest='sl_stop_prob')
    parser.add_argument('--extra-bond-file', default=None, type=str, dest='ex_bond_file')
    parser.add_argument('--restart-path', '--restart_path', default='restart/', dest='restart_path')
    parser.add_argument('--restart-period', '--restart_period', default=5e6, type=float, dest='restart_period')
    parser.add_argument('--remove-overlapp-path','--rop','--remove_overlapp_path', default='./', dest='remove_overlapp_path')
    parser.add_argument('--log-path', '--log_path', default='logs/', dest='log_path')
    parser.add_argument('--log-period', '--log_period', default=5e4, type=float, dest='log_period')
    parser.add_argument('--dt', default=0.01, type=float, dest='dt')
    parser.add_argument('--gamma', default=2.0, type=float, dest='gamma')
    parser.add_argument('--wall', default=False, type=bool, dest='wall')
    parser.add_argument('--hoomd','--hoomd_args', default='cpu', dest='hoomd_args', type=str)
    args = parser.parse_args()
    return args

##############################
## logging functions ##
##############################
def calc_R2(timstep):
    snap = system.take_snapshot(particles=True, bonds=False, integrators=False)
    ids = snap.particles.typeid==0
    res = saw.R2(snap.particles.position[ids], snap.particles.image[ids], D)
    return res

def calc_R_G2(timestep):
    snap = system.take_snapshot(particles=True, bonds=False, integrators=False)
    ids = snap.particles.typeid==0
    res = saw.R_G2(snap.particles.position[ids], snap.particles.image[ids], D)
    return res
